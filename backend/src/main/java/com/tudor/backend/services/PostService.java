package com.tudor.backend.services;

import com.tudor.backend.dto.PostDTO;
import com.tudor.backend.entities.Post;
import com.tudor.backend.repositories.PostRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostService {
    @Autowired
    PostRepository postRepository;

    @Transactional
    public ResponseEntity<?> getAllPosts() {
        return new ResponseEntity<>(postRepository.findAll(), new HttpHeaders(), HttpStatus.OK);
    }

    @Transactional
    public Post createPost(PostDTO postDTO) {
        var post = new Post();
        post.setTitlu(postDTO.getTitlu());
        post.setText(postDTO.getText());
        return postRepository.save(post);
    }

    @Transactional
    public void deletePostById(int id) {
        postRepository.deleteById(id);
    }
}
