package com.tudor.backend.services;

import com.tudor.backend.dto.UserDTO;
import com.tudor.backend.entities.User;
import com.tudor.backend.repositories.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Transactional
    public ResponseEntity<?> verifyLogin(UserDTO userDTO) {
        User user = userRepository.findByNameAndPassword(userDTO.getName(),
                userDTO.getPassword());

        if (user != null) {
            return new ResponseEntity<>(userDTO, new HttpHeaders(), HttpStatus.OK);
        }
        return new ResponseEntity<>(userDTO, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Transactional
    public ResponseEntity<?> getAllAccounts() {
        return new ResponseEntity<>(userRepository.findAll(), new HttpHeaders(),
                HttpStatus.OK);
    }

    @Transactional
    public User registerUser(UserDTO userDTO) {
        var user = new User();
        user.setName(userDTO.getName());
        user.setPassword(userDTO.getPassword());
        return userRepository.save(user);
    }

    @Transactional
    public void deleteUserById(int id) {
        userRepository.deleteById(id);
    }
}
