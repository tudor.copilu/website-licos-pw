package com.tudor.backend.repositories;

import com.tudor.backend.entities.Post;
import com.tudor.backend.entities.User;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends CrudRepository<Post, Integer> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Iterable<Post> findAll();

}
