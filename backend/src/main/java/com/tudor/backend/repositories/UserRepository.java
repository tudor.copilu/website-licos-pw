package com.tudor.backend.repositories;

import com.tudor.backend.entities.User;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    User findByNameAndPassword(String name, String password);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Iterable<User> findAll();

}
