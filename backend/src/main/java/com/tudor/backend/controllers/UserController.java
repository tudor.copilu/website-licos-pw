package com.tudor.backend.controllers;

import com.tudor.backend.dto.UserDTO;
import com.tudor.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@CrossOrigin(origins = "null")
@RestController
@RequestMapping
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping(path = "/login")
    public ResponseEntity<?> verifyLogin(@RequestBody UserDTO userDTO) {
        return userService.verifyLogin(userDTO);
    }

    @PostMapping(path = "/register")
    public ResponseEntity<?> registerUser(@RequestBody UserDTO userDTO) {
        userService.registerUser(userDTO);
        return new ResponseEntity<>(userDTO, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping(path = "/accounts")
    public ResponseEntity<?> getAllAccounts() {
        return userService.getAllAccounts();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable int id) {
        userService.deleteUserById(id);
        return ResponseEntity.ok().build();
    }
}
