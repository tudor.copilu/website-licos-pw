package com.tudor.backend.controllers;

import com.tudor.backend.dto.PostDTO;
import com.tudor.backend.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "null")
@RestController
@RequestMapping
public class PostController {
    @Autowired
    PostService postService;

    @GetMapping(path = "/posts")
    public ResponseEntity<?> getAllPosts() {
        return postService.getAllPosts();
    }

    @PostMapping(path = "/new_post")
    public ResponseEntity<?> createPost(@RequestBody PostDTO postDTO) {
        postService.createPost(postDTO);
        return new ResponseEntity<>(postDTO, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/delete_post/{id}")
    public ResponseEntity<?> deletePost(@PathVariable int id) {
        postService.deletePostById(id);
        return ResponseEntity.ok().build();
    }
}
