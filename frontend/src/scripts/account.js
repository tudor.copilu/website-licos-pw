function registerUser(username, password) {
    const data = {
        name: username, password: password
    };
    const options = {
        method: 'POST', headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    fetch('http://localhost:8080/register', options)
        .then(response => {
            if (response.ok) {
                console.log('cont creat cu succes');
            } else {
                console.log('nu s-a create cont nou');
            }
        })
        .catch((error) => {
            console.log(error);
        })
}

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('newAccount').addEventListener('submit', function (event) {
        const username = document.getElementById('username').value;
        const password = document.getElementById('password').value;

        registerUser(username, password);
    });
});

function getAllUsers() {
    fetch('http://localhost:8080/accounts')
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            updateAccountList(data);
        })
        .catch(error => {
            console.error('A fost o problema la request-ul catre /accounts: ', error);
        });

}

function updateAccountList(data) {
    const table = document.createElement('table');
    table.style.width = '750px';
    table.style.marginLeft = 'auto';
    table.style.marginRight = 'auto';
    table.setAttribute('border', '1');

    const thead = document.createElement('thead');
    let headerRow = document.createElement('tr');
    ['ID', 'Titlu', 'Text'].forEach(headerText => {
        let header = document.createElement('th');
        header.textContent = headerText;
        headerRow.appendChild(header);
    });
    thead.appendChild(headerRow);
    table.appendChild(thead);

    const tbody = document.createElement('tbody');
    data.forEach(item => {
        let row = document.createElement('tr');
        Object.values(item).forEach(text => {
            let cell = document.createElement('td');
            cell.textContent = text;
            row.appendChild(cell);
        });
        tbody.appendChild(row);
    });
    table.appendChild(tbody);

    const accountListDiv = document.getElementById('accountList');
    accountListDiv.appendChild(table);
}

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('deleteAccount').addEventListener('submit', function (event) {
        const userId = document.getElementById('userId').value;
        deleteUser(userId);
    });
});

function deleteUser(userId) {
    const options = {
        method: 'DELETE', headers: {
            'Content-Type': 'application/json'
        },
    };

    fetch('http://localhost:8080/delete/' + userId, options)
        .then(response => {
            if (response.ok) {
                console.log('cont sters cu succes');
            } else {
                console.log('nu s-a sters contul');
            }
        })
        .catch((error) => {
            console.log(error);
        })
}

function displayPostsSummary(data) {
    const table = document.createElement('table');
    table.style.width = '750px';
    table.style.marginLeft = 'auto';
    table.style.marginRight = 'auto';
    table.setAttribute('border', '1');
    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');
    const tableSection = document.getElementById('postList');

    const headerRow = document.createElement('tr');
    const idHeader = document.createElement('th');
    idHeader.textContent = 'ID';
    const titleHeader = document.createElement('th');
    titleHeader.textContent = 'Title';

    headerRow.appendChild(idHeader);
    headerRow.appendChild(titleHeader);
    thead.appendChild(headerRow);
    table.appendChild(thead);

    data.forEach(newsItem => {
        const row = document.createElement('tr');

        const idCell = document.createElement('td');
        idCell.textContent = newsItem.id;

        const titleCell = document.createElement('td');
        titleCell.textContent = newsItem.titlu;

        row.appendChild(idCell);
        row.appendChild(titleCell);
        tbody.appendChild(row);
    });

    table.appendChild(tbody);
    tableSection.appendChild(table);
}

function getAllPosts() {
    fetch('http://localhost:8080/posts')
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            displayPostsSummary(data);
        })
        .catch(error => {
            console.error('A fost o problema la request-ul catre /posts: ', error);
        });
}

function createPost(title, content) {
    const data = {
        titlu: title,
        text: content
    };
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    fetch('http://localhost:8080/new_post', options)
        .then(response => {
            if (response.ok) {
                console.log('postare creata cu succes');
            } else {
                console.log('nu s-a creat postarea noua');
            }
        })
        .catch((error) => {
            console.log(error);
        })
}

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('newPost').addEventListener('submit', function (event) {
        const title = document.getElementById('titlu').value;
        const content = document.getElementById('continut').value;

        createPost(title, content);
    });
});

function deletePost(postId) {
    const options = {
        method: 'DELETE', headers: {
            'Content-Type': 'application/json'
        },
    };

    fetch('http://localhost:8080/delete_post/' + postId, options)
        .then(response => {
            if (response.ok) {
                console.log('postare stearsa cu succes');
            } else {
                console.log('nu s-a sters postarea');
            }
        })
        .catch((error) => {
            console.log(error);
        })
}

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('deletePost').addEventListener('submit', function (event) {
        const postId = document.getElementById('postId').value;
        deletePost(postId);
    });
});


getAllUsers();
getAllPosts();
