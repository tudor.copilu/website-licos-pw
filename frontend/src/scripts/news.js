function toggleText(element) {
    if (element != null) {
        element.classList.toggle('expanded');
    }
}

function getAllPosts() {
    fetch('http://localhost:8080/posts')
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            displayPosts(data);
        })
        .catch(error => {
            console.error('A fost o problema la request-ul catre /posts: ', error);
        });
}

function displayPosts(data) {
    const newsSection = document.getElementById('newsSection');

    data.forEach(newsItem => {
        const newsDiv = document.createElement('div');
        newsDiv.className = 'news-item';
        newsDiv.setAttribute('onclick', 'this.classList.toggle(\'expanded\');');

        const title = document.createElement('h1');
        title.className = 'news-title';
        title.textContent = newsItem.titlu;

        const text = document.createElement('p');
        text.innerHTML = newsItem.text.replace(/\n/g, '<br>');

        newsDiv.appendChild(title);
        newsDiv.appendChild(text);

        newsSection.appendChild(newsDiv);
    });
}


document.addEventListener('DOMContentLoaded', function () {
    getAllPosts();
}, false);
