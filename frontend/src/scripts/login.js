function checkCredentials(username, password) {
    const data = {
        name: username, password: password
    };
    const options = {
        method: 'POST', headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    fetch('http://localhost:8080/login', options)
        .then(response => {
            if (response.ok) {
                localStorage.setItem("name", username);
                window.location.assign('./accounts.html');
            }
            else {
                localStorage.removeItem("name");
            }
        })
        .catch((error) => {
            console.log(error);
        })
}

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('loginForm').addEventListener('submit', function (event) {
        event.preventDefault();

        const username = document.getElementById('username').value;
        const password = document.getElementById('password').value;

        console.log("Username: " + username);
        console.log("Password: " + password);

        checkCredentials(username, password);
    });
});


// fetch('http://localhost:8080/login')
//     .then(response => {
//         if (!response.ok) {
//             throw new Error('Network response was not ok');
//         }
//         return response.json();
//     })
//     .then(data => {
//         console.log(data);
//     })
//     .catch(error => {
//         console.error('There has been a problem with your fetch operation:', error);
//     });
