const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        login: './src/scripts/login.js',
        account: './src/scripts/account.js',
        news: './src/scripts/news.js'
    },

    output: {
        publicPath: '/public/',
        path: path.resolve(__dirname, 'public'),
        filename: '[name].bundle.js',
    },

    mode: 'development',

    devServer: {
        static: {
            directory: path.join(__dirname, 'public'),
        },
        hot: true,
        compress: true,
        port: 9000,
    },

    module: {
        rules: [
            {test: /\.css$/, use: ['style-loader', 'css-loader'],},
            {test: /\.(png|svg|jpg|jpeg|gif)$/i, type: 'asset/resource',},
        ],
    }
};
